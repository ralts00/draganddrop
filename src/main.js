import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.config.productionTip = false
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    tasks: []
  },
  mutations: {
    addTask (state, task) {
      state.tasks.push (task)
    },
    updateTask (state, [origin, destination]) {
      const taskMoved = state.tasks[origin]
      state.tasks.splice (origin, 1)
      const newDestination = destination + 1
      state.tasks.splice (destination, 0, taskMoved)
      console.log(state.tasks)
      //const taskFiltered = tasksSpliced.splice (destination, 0, taskMoved)
      //state.tasks = Array.from(state.tasks)
    }
  },
  actions: {
    updateTask ({ commit }, [origin, destination]) {
      commit('updateTask', [origin, destination])
    }
  },
  getters: {
    refresh (state) {
      return state.tasks
    }
  },
  plugins: [createPersistedState()]
})

new Vue({
  store: store,
  render: h => h(App),
}).$mount('#app')
